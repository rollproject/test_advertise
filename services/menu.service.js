(function(){

  'use strict';

  angular.module('common.services')
    .factory('menunya', [
      '$location',
      function ($location) {

        var sections = [{
          name: 'Dashboard',
          state: 'home.gettingstarted',
          type: 'link'
        }];

        sections.push({
          name: 'Basic Component',
          type: 'toggle',
          pages: [{
            name: 'Form Layout',
            type: 'link',
            state: 'home.formlayout',
            url:'formlayout',
            icon: 'side-icn note'
          }, {
            name: 'Dialog',
            state: 'home.toollist',
            url:'formdialoglayout',
            type: 'link',
            icon: 'side-icn note'
          },
            {
              name: 'Image',
              state: 'home.createTool',
              type: 'link',
            url:'formlayout',
            icon: 'side-icn note'
            },{
              name: 'Button',
              state: 'home.createTool',
            url:'formlayout',
              type: 'link',
            icon: 'side-icn note'
            },{
              name: 'Message',
              state: 'home.createTool',
            url:'formlayout',
              type: 'link',
            icon: 'side-icn note'
            }]
        });

        sections.push({
          name: 'Extras',
          type: 'toggle',
          pages: [{
            name: 'List Table',
            type: 'link',
            state: 'home.findwood',
            url:'formlayout',
            icon: 'side-icn note'
          }, {
            name: 'Chart',
            state: 'home.woodlist',
            type: 'link',
            url:'formlayout',
            icon: 'side-icn note'
          },
            {
              name: 'Dialog',
              state: 'home.woodlow',
              type: 'link',
            url:'formlayout',
            icon: 'side-icn note'
            }]
        });
        
        sections.push({
          name: 'Angular Jutsu',
          type: 'toggle',
          pages: [{
            name: 'Cheetos',
            type: 'link',
            state: 'home.findwood',
            url:'formlayout',
            icon: 'side-icn note'
          }, {
            name: 'Banana Chips',
            state: 'home.woodlist',
            url:'formlayout',
            type: 'link',
            icon: 'side-icn note'
          },
            {
              name: 'Donuts',
              state: 'home.woodlow',
            url:'formlayout',
              type: 'link',
            icon: 'side-icn note'
            }]
        });


        var self;

        return self = {
          sections: sections,

          toggleSelectSection: function (section) {
            console.log("toggleSelectSection",section);
            self.openedSection = (self.openedSection === section ? null : section);
          },
          isSectionSelected: function (section) {
            //console.log("isSectionSelected","called");
            return self.openedSection === section;
          },

          selectPage: function (section, page) {
            console.log("selectPage",section);
            page && page.url && $location.path(page.url);
            self.currentSection = section;
            self.currentPage = page;
          }
        };
      }])

})();