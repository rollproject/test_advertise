(function () {
    'use strict';

    angular.module('PromoLandingPage', ['MainActivity','ProfileController', 'ngMaterial', 'ngAnimate',
        'ui.router', 'ngMessages',
        'ngAria'
    ])
    /*.config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('purple', {
                'default': '300'
            })
            .accentPalette('deep-orange', {
                'default': '500'
            });
    })*/
        .config(function ($mdThemingProvider) {
            $mdThemingProvider.theme('none');
        })
        .config(['$stateProvider', '$urlRouterProvider', '$logProvider',
            function ($stateProvider, $urlRouterProvider) {

                //$urlRouterProvider.otherwise("/main/dashboard");

                $stateProvider
                    .state('mainstate', {
                        url: '/main',
                        templateUrl: 'views/wrapper_layout.html',
                        //controller: 'HomeCtrl as vm'
                        controller: 'HomeCtrl'
                        /*views: {
                            '@': {
                                templateUrl: 'views/wrapperlayout.html',
                                controller: 'HomeCtrl as vm'
                            }
                        }*/
                    })
                    .state('mainstate.home', {
                        url: '/home',
                        views: {
                            /*'header': {
                                templateUrl: 'views/header_promo1_layout.html',
                                controller: 'FrontCtrl',
                            },*/
                            'body': {
                                templateUrl: 'views/promo1.html',
                            }
                        }
                        //controller: 'FormLayoutCtrl',
                    })
                    .state('mainstate.foodpromo', {
                        url: '/foodpromo',
                        views: {
                            /*'header': {
                                templateUrl: 'views/header_promo1_layout.html',
                                controller: 'FrontCtrl',
                            },*/
                            'body': {
                                templateUrl: 'views/promo2.html',
                            }
                        }
                        //controller: 'FormLayoutCtrl',
                    })
                    .state('paralax', {
                        url: '/paralax',
                        templateUrl: 'views/paralax_layout.html',
                        controller: 'HomeCtrl'
                    })
                    .state('paralax.number1', {
                        url: '/number1',
                        controller: 'HomeCtrl',
                        views: {
                            'body': {
                                templateUrl: 'views/paralax/pnumber1.html',
                            }
                        }
                    })
                    .state('profilecompany', {
                        url: '/profilecompany',
                        templateUrl: 'views/profile_layout.html',
                        controller: 'HomeCtrl'
                    })
                    .state('profilecompany.profile1', {
                        url: '/profile1',
                        views: {
                            'body': {
                                templateUrl: 'views/profilecompany/profile1.html',
                                controller: 'ProfileCtrl'
                            }
                        }

                    })

            }
        ])
    //take all whitespace out of string
        .filter('nospace', function () {
            return function (value) {
                return (!value) ? '' : value.replace(/ /g, '');
            };
        })
    //replace uppercase to regular case
        .filter('humanizeDoc', function () {
            return function (doc) {
                if (!doc) return;
                if (doc.type === 'directive') {
                    return doc.name.replace(/([A-Z])/g, function ($1) {
                        return '-' + $1.toLowerCase();
                    });
                }

                return doc.label || doc.name;
            };
        })
        .controller('MainActivity', ['$scope', '$http', '$mdSidenav', function ($scope, $http, $mdSidenav) {

        }]);
})();