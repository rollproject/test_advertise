/**
 * Created by donny on 12/19/2015.
 */
(function () {
    'use strict';
    angular.module('MainActivity')
        .controller('HomeCtrl', [
            '$rootScope',
            '$log',
            '$state',
            '$mdSidenav',
            '$timeout',
            '$location',
            'menunya',
            function ($rootScope, $log, $state, $mdSidenav, $timeout, $location, menunya) {
                var vm = this;
                var aboutMeArr = ['Family', 'Location', 'Lifestyle'];
                var budgetArr = ['Housing', 'LivingExpenses', 'Healthcare', 'Travel'];
                var incomeArr = ['SocialSecurity', 'Savings', 'Pension', 'PartTimeJob'];
                var advancedArr = ['Assumptions', 'BudgetGraph', 'AccountBalanceGraph', 'IncomeBalanceGraph'];


                $rootScope.toggleLeft = buildToggler('left');
                
                //functions for menu-link and menu-toggle
                vm.isOpen = isOpen;
                vm.toggleOpen = toggleOpen;
                vm.isSectionSelected = isSectionSelected;
                vm.autoFocusContent = false;
                vm.menu = menunya;
                vm.urlhref = urlhref;
                vm.hideSideMenu = $rootScope.toggleLeft;
                console.log('menu: ', vm.menu)

                console.log('section: ', vm.menu.sections)

                console.log('scope', $rootScope)

                vm.status = {
                    isFirstOpen: true,
                    isFirstDisabled: false
                };
                function isOpen(section) {
                    //console.log("isOpenController","called "+menunya.isSectionSelected(section));
                    return menunya.isSectionSelected(section);
                }

                function toggleOpen(section) {
                    menunya.toggleSelectSection(section);
                }

                function isSectionSelected(section) {
                    var selected = false;
                    var openedSection = menunya.openedSection;
                    if (openedSection === section) {
                        selected = true;
                    }
                    else if (section.children) {
                        section.children.forEach(function (childSection) {
                            if (childSection === openedSection) {
                                selected = true;
                            }
                        });
                    }
                    return selected;
                }

                function urlhref(url) {
                    $location.path(url);
                    //$rootScope.$apply();
                    // $location.path('/path');
                    if (!$rootScope.$$phase) {
                        $rootScope.$apply();
                        //console.log('test',"tesssssssssssssssst");
                    }
                }

                var onLoad = function () {
                    $location.path("/main/dashboard");
                    //$rootScope.$apply();
                    // $location.path('/path');
                    if (!$rootScope.$$phase) {
                        $rootScope.$apply();
                        console.log('test', "tesssssssssssssssst");
                    }
                }


                function buildToggler(navID) {
                    return function () {
                        $mdSidenav(navID)
                            .toggle()
                            .then(function () {
                                $log.debug("toggle " + navID + " is done");
                            });
                    }
                }
                console.log("start", "called");
                
                //onLoad();
            }
        ])
})();
