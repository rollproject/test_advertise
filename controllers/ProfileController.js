(function () {
    'use strict';
    angular.module('ProfileController')
        .controller('ProfileCtrl', [
            '$rootScope',
            '$scope',
            '$log',
            '$interval',
            '$timeout',
            '$element',
            function ($rootScope, $scope, $log, $interval,$timeout, $element) {
                var imagedata = [{
                    'url': 'images/image-items/carousel_bg.png'
                }, {
                        'url': 'images/image-items/carousel_bg.png'
                    }, {
                        'url': 'images/image-items/carousel_bg.png'
                    }];
                var stepLeft = 0;
                for (var b = 0; b < imagedata.length; b++) {
                    if (b == 0) {
                        stepLeft = 0;
                    } else {
                        stepLeft += 101;
                    }
                    imagedata[b].left = stepLeft;
                    imagedata[b].id = 'carousel' + b;
                }
                $scope.mycarousel = imagedata;
                var timeoutnya = 3000;
                var isRunning = false;
                function intervalCarousel() {
                    if(!isRunning){
                        isRunning = true;
                        for (var a = 0; a < imagedata.length; a++) {
                            if(a == 0){
                                var tempatdata = null;
                                $($element).find('#' + imagedata[a].id).animate({
                                left: "-=" +diffCarouselItem(imagedata[a],101)  + "%"
                                }, { duration: 1000, queue: false,complete:function () {
                                    // Animation complete.
                                    $($element).find('#'+imagedata[0].id).css("left",imagedata[0].left+"%");
                                    
                                    tempatdata = imagedata[0];
                                    imagedata.splice(0, 1);
                                    imagedata.push(tempatdata);
                                    isRunning = false;
                                    //call begin
                                    $timeout(intervalCarousel, timeoutnya);
                                } });
                                
                            }else{
                                $($element).find('#' + imagedata[a].id).animate({
                                left: "-=" +diffCarouselItem(imagedata[a],101)  + "%"
                                }, { duration: 1000, queue: false,complete:function () {
                                    // Animation complete.
                                } });
                            }   
                        }
                    }
                }
                
                function diffCarouselItem(bbb,minus){
                    var left = bbb.left - minus;
                    if(left < 0){
                        left = minus;
                        bbb.left = stepLeft;
                        return left;
                    }else if(left == 0){
                        left = minus;
                        bbb.left = minus;
                        return left;
                    }else{
                        bbb.left = left;
                        return left;
                    }
                }
                
                //running now
                $timeout(intervalCarousel, timeoutnya);
                
                var isClicked = false;
                $scope.clickmenu = function(){
                    if(!isClicked){
                        isClicked = true;
                        $($element).find('.wrap-mobile-menu').css("background","#2e2b34");
                        $($element).find('.content-menu').css("display","table");
                    }else{
                        isClicked = false;
                        $($element).find('.wrap-mobile-menu').css("background","transparent");
                        $($element).find('.content-menu').css("display","none");
                    }
                }
            }
        ])
})();