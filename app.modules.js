(function(){
  'use strict';
  
  angular.module('common.services', []);
  angular.module('MainActivity', ['common.directives']);
  angular.module('common.directives',['common.services'])
  angular.module('ProfileController',[]);
  
})();